package com.provisota.diceroller

import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    companion object {
        const val player1InviteText = "Player 1 push the ROLL button"
        const val player2InviteText = "Player 2 push the ROLL button"
        const val restartInviteText = "Push the button to play again"
        const val rollButtonLabel = "ROLL"
        const val restartButtonLabel = "RESTART"

        const val player1WinText = "Player 1 WINS!"
        const val player2WinText = "Player 2 WINS!"
        const val noWinnerText = "No winner, play again."

        val drawableResources = listOf(
            R.drawable.dice_1,
            R.drawable.dice_2,
            R.drawable.dice_3,
            R.drawable.dice_4,
            R.drawable.dice_5,
            R.drawable.dice_6
        )
    }

    private var spinCounter = 0
    private var player1Result = 0
    private var player2Result = 0

    private lateinit var inviteText: TextView
    private lateinit var winnerLabel: TextView

    private lateinit var player1dice1Image: ImageView
    private lateinit var player1dice2Image: ImageView

    private lateinit var player2dice1Image: ImageView
    private lateinit var player2dice2Image: ImageView

    private lateinit var rollButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // TODO add support of the SCREEN_ORIENTATION_LANDSCAPE
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        initReferences()

        rollButton.setBackgroundColor(Color.BLUE)
        rollButton.setOnClickListener { rollDice() }
    }

    private fun initReferences() {
        inviteText = findViewById(R.id.invite_text)
        winnerLabel = findViewById(R.id.winner_label)

        player1dice1Image = findViewById(R.id.player1_dice1_image)
        player1dice2Image = findViewById(R.id.player1_dice2_image)

        player2dice1Image = findViewById(R.id.player2_dice1_image)
        player2dice2Image = findViewById(R.id.player2_dice2_image)

        rollButton = findViewById(R.id.roll_button)
    }

    private fun rollDice() {
        val randomInt1 = (1..6).random()
        val randomInt2 = (1..6).random()

        val drawableResource1 = drawableResources[randomInt1 - 1]
        val drawableResource2 = drawableResources[randomInt2 - 1]

        when (spinCounter) {
            0 -> {
                player1Result = randomInt1 + randomInt2

                player1dice1Image.setImageResource(drawableResource1)
                player1dice2Image.setImageResource(drawableResource2)

                inviteText.text = player2InviteText

                spinCounter++
            }
            1 -> {
                player2Result = randomInt1 + randomInt2

                player2dice1Image.setImageResource(drawableResource1)
                player2dice2Image.setImageResource(drawableResource2)

                inviteText.text = restartInviteText

                rollButton.text = restartButtonLabel
                rollButton.setBackgroundColor(Color.parseColor("#4CAF50"))

                winnerLabel.text =
                    when {
                        player1Result > player2Result -> player1WinText
                        player2Result > player1Result -> player2WinText
                        else -> noWinnerText
                    }
                spinCounter++
            }
            2 -> {
                player1Result = 0
                player2Result = 0
                spinCounter = 0

                player1dice1Image.setImageResource(R.drawable.empty_dice)
                player1dice2Image.setImageResource(R.drawable.empty_dice)

                player2dice1Image.setImageResource(R.drawable.empty_dice)
                player2dice2Image.setImageResource(R.drawable.empty_dice)

                inviteText.text = player1InviteText

                rollButton.text = rollButtonLabel
                rollButton.setBackgroundColor(Color.BLUE)

                winnerLabel.clearText()
            }
        }

    }

}

private fun TextView.clearText() = setText("")
